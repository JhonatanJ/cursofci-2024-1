import numpy as np
class tiroparabolico:
    """
    G=gravedad
    angle= ángulo inicial de salida
    velini= magnitud de la velocidad inicial de salida
    h_0= altura inicial
    x_0= posición inicial en x


    """

    def __init__(self,G,angle,velini,h_0,x_0):
        self.G=G
        self.a=angle
        self.v=velini
        self.h=h_0
        self.x=x_0

    def vel_x(self,t):
        return self.v*np.cos(self.a)
    
    def pos_x(self,t):
        return self.v*np.cos(self.a)*t+self.x

    def vel_y(self,t):
        return self.v*np.sin(self.a)+G*t
    
    def pos_y(self,t):
        return self.v*np.sin(self.a)*t+(self.G/2)*(t**2)+self.h 
        
    def tiempo_max_vuelo(self):
        g=self.G
        th=self.a
        y_0=self.h
        v1=self.v
        return (-((v1*np.sin(th))**2-2*g*y_0)**(0.5)-(v1*np.sin(th)))/g