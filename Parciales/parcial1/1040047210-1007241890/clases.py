import numpy as np

class RKG():
    '''------------------ Runge-kutta generalizado  --------------------
    resuelve un sistema con varias ecuaciones diferenciales (ode)

    Args:
    f: Función que define la ecuación diferencial y' = f(t, y).
    y: condiciones iniciales de la función y(t0). (array)
    b: Valor final de la variable independiente.
    h: tamaño de paso de la variable independiente.
    s: Orden de método Runge Kutta (número de etapas)
    c_c: array con las constantes c para el método
    c_a: matriz con las constantes a para el método
    c_b: array con las constantes b para el método

    '''

    def __init__(self,f,y,b,h, s, c_c, c_a, c_b):
      self.f = f
      self.y = y
      self.b = b
      self.h = h
      self.s = s
      self.c_c = c_c
      self.c_a = c_a
      self.c_b = c_b

    def ks(self, y, t):
      """
      Calcula los valores k_i en cada etapa del método de Runge-Kutta generalizado.

      :param y: Valor actual de la variable dependiente.
      :param t: Tiempo actual.
      :return: Lista de valores k_i.
      """

      k_values = []
      for i in range(len(self.c_b)):
        sum_term = sum(self.c_a[i][j] * k_j for j, k_j in enumerate(k_values))
        k = self.f(t + self.c_c[i] * self.h, y + self.h * sum_term)
        k_values.append(k)
      return k_values

    def soluciones(self, t0, y0):
        """
        Método para resolver una EDO utilizando el método de Runge-Kutta generalizado.

        :param y0: Valor inicial de la variable dependiente.
        :param t0: Tiempo inicial.
        :return: Lista de valores de la variable dependiente en cada paso de tiempo.
        """
        n = (self.b - t0)/self.h #El número de pasos a tomar para discretizar el intervalo de tiempo
        n = int(n)
        t_values = [t0]
        y_values = [y0]
        tt = 1
        while tt <= n:
            k = self.ks(y0, t0)
            y0 += self.h * sum(b * k_i for b, k_i in zip(self.c_b, k))
            t0 += self.h
            tt += 1
            t_values.append(t0)
            y_values.append(y0)

        return t_values, y_values
        

class RKGA(RKG):
   
    def __init__(self, f, y, b, h, s, c_c, c_a, c_b):
      super().__init__(f, y, b, h, s, c_c, c_a, c_b)

    def ks_sis(self, y, t):
      """
      Calcula los valores k_i en cada etapa del método de Runge-Kutta generalizado.

      :param y: Valor actual de la variable dependiente.
      :param t: Tiempo actual.
      :return: Lista de valores k_i.
      """    
      k_x = []
      k_y = []
      k_z = []
      for i in range(len(self.c_b)):
        sum_termx = sum(self.c_a[i][j] * k_j for j, k_j in enumerate(k_x))
        sum_termy = sum(self.c_a[i][j] * k_j for j, k_j in enumerate(k_y))
        sum_termz = sum(self.c_a[i][j] * k_j for j, k_j in enumerate(k_z))
        k = self.f(t + self.c_c[i] * self.h, [y[0] + self.h * sum_termx, y[1] + self.h * sum_termy, y[2] + self.h * sum_termz])
        k_x.append(k[0])
        k_y.append(k[1])
        k_z.append(k[2])
      return k_x, k_y, k_z

      
    def sol_sistem(self, t0):
      """
      Método para resolver una EDO utilizando el método de Runge-Kutta generalizado.

      :param y0: Valor inicial de la variable dependiente.
      :param t0: Tiempo inicial.
      :return: Lista de valores de la variable dependiente en cada paso de tiempo.
      """
      n = (self.b - t0)/self.h #El número de pasos a tomar para discretizar el intervalo de tiempo
      n = int(n)
      t_values = [t0]
      x_values = [self.y[0]]
      y_values = [self.y[1]]
      z_values = [self.y[2]]
      suma_x = 1
      tt = 1
      while tt <= n:
          k_x,k_y,k_z = self.ks_sis(self.y, t0)
          self.y[0] += self.h * sum(b * k_i for b, k_i in zip(self.c_b, k_x))
          self.y[1] += self.h * sum(b * k_i for b, k_i in zip(self.c_b, k_y))
          self.y[2] += self.h * sum(b * k_i for b, k_i in zip(self.c_b, k_z))
          t0 += self.h
          tt += 1
          t_values.append(t0)
          x_values.append(self.y[0])
          y_values.append(self.y[1])
          z_values.append(self.y[2])
      return np.array(t_values), np.array(x_values), np.array(y_values),np.array(z_values)