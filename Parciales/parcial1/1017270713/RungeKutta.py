import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

class RKG:
    """Clase RKG para implementar el método de Runge-Kutta generalizado.
    
        Atributos:
        - orden = s (float): Asigna el orden deseado para el método
        
        - a = a (np.ndarray): En lugar de la matriz de Runge-Kutta completa, se toman solamente
                              los valores pertinentes para el cálculo de las constantes k.
        
        - pesos = b (np.ndarray): Los pesos en la cuadratura del método de Runge-Kutta generalizado.
        
        - nodos = c (np.ndarray): Los nodos en el paso para la variable independiente en el método de
                                  Runge-Kutta generalizado.
                                  
        Métodos:
        - ks: Calcula los valores de las constantes k para el orden deseado.
        
        - soluciones: Se implementa la integración de las soluciones para la EDO.
        
        - convergencia: Calcula y compara el error de la implementación con la solución exacta.
        
        - plots: Realiza diferentes gráficas, dependiendo de la opción elegida.
        """
    def __init__(self, s, a, b, c):
        self.orden = s  # Orden del método de Runge-Kutta
        self.a = a  # Coeficientes de la matriz de Runge-Kutta
        self.pesos = b  # Pesos de la cuadratura
        self.nodos = c  # Nodos de la iteración

    def ks(self, funcion, x, y, h):
        """Calcula las k para el orden deseado y deja listo un array con esos valores.
        Entrada:
        - x: Array que define la variable independiente del problema.
        - y: Array que define la variable dependiente del problema.
        - h: flotante que define el tamaño del paso en la integración.
        
        Retorna: Array con todos los valores de k para el orden deseado
        """
        k = np.zeros(self.orden) #Definimos el array donde se almacenan los valores de k
        suma = 0
        for j in range(self.orden):
            k[j] = funcion(x + self.nodos[j] * h, y + h * suma) #Actualizamos el array de k
            suma += self.a[j] * k[j] #Actualizamos el valor de la suma
        
        return k

    def soluciones(self, funcion, x0, y0, xf, h):
        """Implementa el algoritmo para solucionar la ecuación diferencial.
        Entrada:
        - funcion: Función derivada que define la EDO del problema.
        - x0: Flotante que define la condición inicial de la variable independiente.
        - y0: Flotante que define la condición inicial de la variable dependiente.
        - xf: Flotante que define hasta dónde se realiza la integración.
        - h: flotante que define el tamaño del paso en la integración.
        
        Retorna: Dos arrays, el intervalo de integración, y la solución de la EDO en dicho intervalo.
        """
        x = np.arange(x0, xf + h, h) #Definimos el intervalo de integración
        y = np.zeros_like(x) #Definimos el array donde se almacena la solución
        y[0] = y0 #Imponemos la condición inicial
        for j in range(len(x) - 1):
            k = self.ks(funcion, x[j], y[j], h) #Calculamos el valor de k para la cuadratura
            y[j+1] = y[j] + h*sum(self.pesos[i] * k[i] for i in range(self.orden)) #Actualizamos la sln
        
        return x, y
    
    def convergencia(self, funcion, x0, y0, xf):
        """Calcula el error absoluto entre la solución exacta y la solución por el método RKG.
        Entrada:
        - funcion: Función derivada que define la EDO del problema.
        - x0: Flotante que define la condición inicial de la variable independiente.
        - y0: Flotante que define la condición inicial de la variable dependiente.
        - xf: Flotante que define hasta dónde se realiza la integración.
        
        Retorna: Las gráficas de convergencia pertinentes (None).
        """
        f = lambda x: x - 1 + np.exp(-x) #Solución exacta con condición inicial y(0) = 0
        y1_difference = []
        y_difference = []
        H = [0.0001, 0.001, 0.01, 0.1] #Diferentes valores del paso de integración
        for j in H:
           sol = self.soluciones(funcion, x0, y0, xf, j)
           exactSol = f(sol[0])
           ys = odeint(funcion, y0, sol[0])
           ys = np.array(ys).flatten()
           y1_difference.append(np.mean(np.abs(ys - exactSol))) #Para cada paso, calculamos el error
           y_difference.append(np.mean(np.abs(ys - sol[1]))) #Para cada paso, calculamos el error
        #Graficación
        plt.loglog(H, y1_difference, "b")
        plt.semilogy(H, y_difference, "r")
        plt.xlabel('h')
        plt.ylabel('err')
        plt.title("Gráfica de convergencia")
        plt.grid(True)
        plt.savefig("Grafica de convergencia.png")
    
    def plots(self, sol, option = "2d"):
       """Plotea las gráficas correspondientes al problema.
       Entrada:
       - sol: Array de soluciones a graficar.
       - option: String que define si se hará una gráfica 2d o 3d.
       
       Retorna: Realiza la graficación esperada (None).
       """
       #Si queremos graficar los resultados de la clase RKG, entonces tendremos una sol con dos arrays
       if len(sol) == 2:
          plt.plot(sol[0], sol[1])
          plt.xlabel('x')
          plt.ylabel('y')
          plt.title("Solución de la ecuación diferencial dy/dx = x - y")
          plt.grid(True)
          plt.show()
       #Si queremos graficar los resultados de la clase RKGa, entonces tendremos una sol con cuatro arrays
       elif len(sol) == 4:
          #Si queremos hacer las gráficas 2d, entonces option == "2d"
          if option == "2d":
             fig, ax = plt.subplots(1, 3, figsize=(10,4)) #Creamos una matriz de tres gráficas
             ax[0].plot(sol[1], sol[2])
             ax[0].set_title('x vs y')
             ax[0].set_xlabel('x')
             ax[0].set_ylabel('y')
             ax[0].grid(True)

             ax[1].plot(sol[1], sol[3])
             ax[1].set_title('x vs z')
             ax[1].set_xlabel('x')
             ax[1].set_ylabel('y')
             ax[1].grid(True)

             ax[2].plot(sol[2], sol[3])
             ax[2].set_title('y vs z')
             ax[2].set_xlabel('x')
             ax[2].set_ylabel('y')
             ax[2].grid(True)
             
             fig.suptitle("Gráficas 2d de las ecuaciones acopladas")
             plt.tight_layout()
             plt.show()
          #Si queremos hacer las gráficas 3d, entonces option == "3d"
          elif option == "3d":
             ax = plt.figure().add_subplot(projection="3d")
             ax.plot(sol[1], sol[2], sol[3])
             ax.set_xlabel('x')
             ax.set_ylabel('y')
             ax.set_zlabel('z')
             ax.set_title('Gráfica 3d con las soluciones de las ecuaciones acopladas')
             plt.show()


class RKGa(RKG):
    """La clase hereda exactamente los mismos atributos y métodos de la clase padre RKG."""
    def __init__(self, s, a, b, c):
        super().__init__(s, a, b, c)

    def ks(self, funcion, args, x, y, h):
        """El método hijo, añade una nueva variable args, que sirve para las funciones vectoriales
           correspondientes a las ecuaciones diferenciales acopladas. En lugar de un array con los
           valores de k, retorna una matriz donde cada columna contiene los valores de k para cada
           variable dependiente."""
        #Para las EDO acopladas, k se define como una matriz en lugar de un array vectorial
        k = np.zeros((self.orden, len(y))) 
        suma = np.zeros(len(y))
        for i in range(3):
          for j in range(self.orden):
            k[:,i][j] = funcion(x + self.nodos[j] * h, y + h * suma, args)[i]
            suma += self.a[j] * k[:,i][j]
        
        return k

    def soluciones(self, funcion, args, t0, y_0, tf, h):
      """Se hace el mismo cambio en cuanto la variable args que en el método hijo ks. Retorna cuatro
         arrays, el intervalo de integración y las tres soluciones de la ecuación acoplada."""
      #Para las EDO acopladas, y se define como una matriz en lugar de un array vectorial para la sol
      t = np.arange(t0, tf + h, h) #Definimos el intervalo de integración
      y = np.zeros((len(t), 3))
      y[0] = y_0
      for j in range(len(t) - 1):
        k = self.ks(funcion, args, t[j], y[j], h)
        for i in range(3):
          y[:,i][j+1] = y[:,i][j] + h * sum(self.pesos[m] * k[:,i][m] for m in range(self.orden))

      return t, y[:,0], y[:,1], y[:,2]