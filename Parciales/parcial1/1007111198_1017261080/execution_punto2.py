from punto2 import RKGA
import numpy as np
import matplotlib.pyplot as plt

if __name__=='__main__':

    # Constantes de Butcher para RK4
    a = [[0, 0, 0, 0], [0.5, 0, 0, 0], [0, 0.5, 0, 0], [0, 0, 1, 0]]
    b = [1/6, 1/3, 1/3, 1/6]
    c = [0, 0.5, 0.5, 1]
    constantes_butcher = [a, b, c]
    # Parámetros de la función a llamar:
    # Función derivada:
    def derivada(x,y):
        return x-y
    
    x0 = 0 # POsición inicial de la variable independiente
    y0 = 0 # Posición inicial de la variable dependiente
    xf = 1 # Posición final de la variable independiente
    h = np.array([0.1,0.01,0.001,0.0001,0.00001]) # Paso del integrador
    rkga_images = []
    for i in h:
        rkga_images.append(RKGA(derivada,x0,y0,xf,i,constantes_butcher).graficas_solicitadas())
        
    
        
    
    

    